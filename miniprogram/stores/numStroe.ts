import { action, observable } from 'mobx-miniprogram'
export const numStore = observable({
  // state
  numA: 1,
  numB: 2,
  // 修改state
  update: action(function (this:any) {
     this.numA+=2
     this.numB+=2
  }),
  // state进行计算
  get numComputed(){
    return this.numA +this.numB
  }
})