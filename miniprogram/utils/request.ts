import WxRequest from "./http"

let instance = new WxRequest({
  baseUrl: 'http://localhost:8080',
  timeout: 5000
})
// 请求拦截器
instance.interceptors.request=(config)=>{
  let token = wx.getStorageSync('token')
  if(token){
    config.header.token = token
  }
  return config;
}
// 响应拦截器
instance.interceptors.response=(response)=>{
  let {isSuccess,data} = response
  if(!isSuccess){
    wx.showToast({
      title:'网络错误请重试',
      icon:'error'
    })
    return response
  }
  return data
}
export default instance;