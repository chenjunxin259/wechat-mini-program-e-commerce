export const formatTime = (date: Date) => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return (
    [year, month, day].map(formatNumber).join('/') +
    ' ' +
    [hour, minute, second].map(formatNumber).join(':')
  )
}

const formatNumber = (n: number) => {
  const s = n.toString()
  return s[1] ? s : '0' + s
}
interface toast{
  title:string,
  icon?:'none'|'success'|'error'|"loading",
  duration?:number,
  mask?:boolean
}
const toast = (toast:toast) => {
  wx.showToast({
    title:toast.title,
    icon:toast.icon,
    duration:toast.duration,
    mask:toast.mask
  })
}
const modal = (options = {}) => {
  return new Promise((reslove) => {
    let opt = {
      title: '提示',
      content: '这个是一个模态框',
    }
    let newOpt = Object.assign({}, opt, options)
    wx.showModal({
      ...newOpt,
      success(res) {
        res.confirm && reslove(true)
        res.cancel && reslove(false)
      }
    })
  })
}
export { toast ,modal}
