export const syncSetStorage = (key: string, value: string) => {
  try {
    wx.setStorageSync(key, value)
  } catch (e) {
    console.log('同步存储失败');
  }
}

export const syncGetStorage = (key: string) => {
  try {
    let res = wx.getStorageSync(key)
    if (res) {
      return res
    }
  } catch (e) {
    console.log('同步获取失败');
  }
}

export const syncRemoveStorage = (key: string) => {
  try {
    wx.removeStorageSync(key)
  } catch (e) {
    console.log('同步删除失败');
  }
}
export const syncClearStorage = () => {
  try {
    wx.clearStorage()
  } catch (e) {
    console.log('同步清空失败');
  }
}

export const setStorage = (key: string, data: string) => {
  return new Promise((reslove) => {
    wx.setStorage({
      key,
      data,
      complete(res) {
        reslove(res)
      }
    })
  })
}

export const getStorage = (key: string) => {
  return new Promise((reslove) => {
    wx.getStorage({
      key,
      complete(res) {
        reslove(res)
      }
    })
  })
}

export const removeStorage = (key: string) => {
  return new Promise((reslove) => {
    wx.removeStorage({
      key,
      complete(res) {
        reslove(res)
      }
    })
  })
}