class WxRequest {
  defaultOptions = {
    baseUrl: '',
    method: 'GET',
    data: null,
    header: {
      "Content-type": "application/json"
    },
    timeout: 6000,
    // 是否显示loading效果
    isLoading: true
  }
  // 解决并发异步任务loading加载效果问题
  quene: string[] = []
  // 解决依赖异步任务loading加载问题
  timeID: number = 0
  // 拦截器
  interceptors = {
    request: (config: any) => config,
    response: (options: any) => options
  }
  // 构造器
  constructor(options: any) {
    Object.assign({}, this.defaultOptions, options)
  }
  // 实例方法
  request(options: any) {
    this.timeID && clearTimeout(this.timeID)
    options.url = this.defaultOptions.baseUrl + options.url
    options = Object.assign(this.defaultOptions, options)
    if (options.isLoading) {
      this.quene.length === 0 && wx.showLoading({ title: '加载中' })
      this.quene.push('request')
    }

    // 配置请求拦截器
    options = this.interceptors.request(options)
    // 封装请求
    return new Promise((reslove, reject) => {
      wx.request({
        ...options,
        success: (res) => {
          let mergeRes = Object.assign({}, res, { config: options, isSuccess: true })
          reslove(this.interceptors.response(mergeRes))
        },
        fail: (err) => {
          let mergeErr = Object.assign({}, err, { config: options, isSuccess: false })
          reslove(this.interceptors.response(mergeErr))
          reject(err)
        },
        complete: () => {
          if (options.isLoading) {
            this.quene.pop()
            this.quene.length === 0 && this.quene.push('request')
            this.timeID = setTimeout(() => {
              this.quene.pop()
              this.quene.length === 0 && wx.hideLoading()
              clearTimeout(this.timeID)
            }, 1)
          }
        }
      })
    })
  }
  get(url: string, data = {}, config = {}) {
    return this.request(Object.assign({ url, method: 'GET' }, data, config))
  }
  post(url: string, data = {}, config = {}) {
    return this.request(Object.assign({ url, method: 'POST' }, data, config))
  }
  put(url: string, data = {}, config = {}) {
    return this.request(Object.assign({ url, method: 'PUT' }, data, config))
  }
  delete(url: string, data = {}, config = {}) {
    return this.request(Object.assign({ url, method: 'DELET' }, data, config))
  }
  // 使用的时候只需要将异步任务传递，展开运算符会自动变成数组类型，结果是一个数组，包含了每个异步任务的结果
  all(...promise: any[]) {
    return Promise.all(promise)
  }
}

export default WxRequest;