import { toast } from "../../../utils/util"
interface toastA {
  title: string,
  icon?: 'none' | 'success' | 'error' | "loading",
  duration?: number,
  mask?: boolean
}
Page({
 
  data: {
    hotCategory: [
      {
        img: 'https://fastly.picsum.photos/id/915/50/50.jpg?hmac=4HSVT8i7NHSiWg5lVXyFbfknvvBvUNYtk-y5DuOroN0',
        name: '分类1'
      },
      {
        img: 'https://fastly.picsum.photos/id/915/50/50.jpg?hmac=4HSVT8i7NHSiWg5lVXyFbfknvvBvUNYtk-y5DuOroN0',
        name: '分类2'
      },
      {
        img: 'https://fastly.picsum.photos/id/915/50/50.jpg?hmac=4HSVT8i7NHSiWg5lVXyFbfknvvBvUNYtk-y5DuOroN0',
        name: '分类3'
      },
      {
        img: 'https://fastly.picsum.photos/id/915/50/50.jpg?hmac=4HSVT8i7NHSiWg5lVXyFbfknvvBvUNYtk-y5DuOroN0',
        name: '分类4'
      },
      {
        img: 'https://fastly.picsum.photos/id/915/50/50.jpg?hmac=4HSVT8i7NHSiWg5lVXyFbfknvvBvUNYtk-y5DuOroN0',
        name: '分类5'
      },
      {
        img: 'https://fastly.picsum.photos/id/915/50/50.jpg?hmac=4HSVT8i7NHSiWg5lVXyFbfknvvBvUNYtk-y5DuOroN0',
        name: '分类6'
      },
      {
        img: 'https://fastly.picsum.photos/id/915/50/50.jpg?hmac=4HSVT8i7NHSiWg5lVXyFbfknvvBvUNYtk-y5DuOroN0',
        name: '分类7'
      },
      {
        img: 'https://fastly.picsum.photos/id/915/50/50.jpg?hmac=4HSVT8i7NHSiWg5lVXyFbfknvvBvUNYtk-y5DuOroN0',
        name: '分类8'
      },
      {
        img: 'https://fastly.picsum.photos/id/915/50/50.jpg?hmac=4HSVT8i7NHSiWg5lVXyFbfknvvBvUNYtk-y5DuOroN0',
        name: '分类9'
      },
      {
        img: 'https://fastly.picsum.photos/id/915/50/50.jpg?hmac=4HSVT8i7NHSiWg5lVXyFbfknvvBvUNYtk-y5DuOroN0',
        name: '分类10'
      }
    ],
    loading: true,
    // 推荐列表
    list: [1, 2, 3] as number[]
  },
  onLoad() {
    setTimeout(() => {
      this.setData({
        loading: false
      })
    }, 1000)
  },

  onReachBottom() {
    let obj: toastA = {
      title: '加载中',
      icon: 'loading',
      duration: 900
    }
    toast(obj)
    setTimeout(() => {
      this.loadMoreData()
    }, 1000)
  },
  generateRandomNumbersInRange: function (count: number) {
    return Array.from({ length: count }, () => Math.floor(Math.random() * 100) + 1);
  },
  loadMoreData: function () {
    let num = this.generateRandomNumbersInRange(3)
    this.setData({
      list: [...this.data.list, ...num]
    })
  }
})
