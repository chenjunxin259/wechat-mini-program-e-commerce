import {BehaviorWithStore } from 'mobx-miniprogram-bindings'
import {numStore} from "../../../stores/numStroe"
export const testBehavior = BehaviorWithStore({
  storeBindings: {
    store:numStore,
    fields: ["numA", "numB", "numComputed"],
    actions: ["update"],
  },
});