// pages/tabbar/cart/cart.ts
// 引入
// import {testBehavior} from './behavier'

Page({
  data: {
    totalMoney: 0,
    switch: false,
    shopList: [
      {
        shopName: '小米自营',
        ifChoose: false,
        chooseVal: 1,
        ifShow: false,
        goodList: [
          {
            ifChoose: false,
            image: 'https://fastly.picsum.photos/id/831/100/100.jpg?hmac=laf9a71pvZ561oyI92OiaOgZoYigZ9cFT4QPbuQMNjw',
            goodName: 'Redmi Watch4',
            goodSpecifications: '印雪白',
            price: 199,
            count: 6,
            chooseVal: 1,
            ifShow: false
          },
          {
            ifChoose: false,
            image: 'https://fastly.picsum.photos/id/831/100/100.jpg?hmac=laf9a71pvZ561oyI92OiaOgZoYigZ9cFT4QPbuQMNjw',
            goodName: 'Redmi Watch4',
            goodSpecifications: '印雪白',
            price: 199,
            count: 6,
            chooseVal: 1,
            ifShow: false
          },
          {
            ifChoose: false,
            image: 'https://fastly.picsum.photos/id/831/100/100.jpg?hmac=laf9a71pvZ561oyI92OiaOgZoYigZ9cFT4QPbuQMNjw',
            goodName: 'Redmi Watch4',
            goodSpecifications: '印雪白',
            price: 199,
            count: 6,
            chooseVal: 1,
            ifShow: false
          },
          {
            ifChoose: false,
            image: 'https://fastly.picsum.photos/id/831/100/100.jpg?hmac=laf9a71pvZ561oyI92OiaOgZoYigZ9cFT4QPbuQMNjw',
            goodName: 'Redmi Watch4',
            goodSpecifications: '印雪白',
            price: 199,
            count: 6,
            chooseVal: 1,
            ifShow: false
          }
        ]
      },
    ]
  },
  onLoad(){
    this.getTotal()
  },
  getTotal(){
    this.data.shopList.forEach((item) => {
      item.goodList.forEach((good) => {
       let price = good.count*good.price
        this.data.totalMoney += price
      })
    });
    this.setData({
      totalMoney: this.data.totalMoney
    })
  },
  checkboxShopChange(e: any) {
    if (e.detail.value.length > 0) {
      this.data.totalMoney = 0
      this.data.shopList.forEach((item) => {
        item.ifChoose = true
        item.goodList.forEach((good) => {
          good.ifChoose = true
          let price = good.count*good.price
          this.data.totalMoney += price
        })
      });
    } else {
      this.data.totalMoney = 0
      this.data.shopList.forEach((item) => {
        item.ifChoose = false

        item.goodList.forEach((good) => {
          good.ifChoose = false
        })
      });
    }
    this.setData({
      shopList: this.data.shopList,
      totalMoney: this.data.totalMoney
    })
  },
  checkboxGoodChange(e: any) {
    let index = e.currentTarget.dataset.index
    if (e.detail.value.length > 0) {
      this.data.shopList.forEach((item) => {
        item.goodList[index].ifChoose = true
        this.data.totalMoney += item.goodList[index].price
      });
    } else {

      this.data.shopList.forEach((item) => {
        item.ifChoose = false
        item.goodList[index].ifChoose = false
        this.data.totalMoney -= item.goodList[index].price
      });
    }
    this.setData({
      shopList: this.data.shopList,
      totalMoney: this.data.totalMoney
    })
  },
  editHandle(e: any) {
    if (e.target.dataset.switch) {
      this.data.shopList.forEach((item) => {
        item.ifShow = false
        item.goodList.forEach((good) => {
          good.ifShow = false
          good.ifChoose = false
          this.data.totalMoney = 0
          this.getTotal()
        })
      });
    } else {
      this.data.shopList.forEach((item) => {
        item.ifShow = true
        item.goodList.forEach((good) => {
          good.ifShow = true
        })
      });
    }
    this.data.switch = !e.target.dataset.switch
    this.setData({
      shopList: this.data.shopList,
      switch: this.data.switch,
      totalMoney: this.data.totalMoney,
    })
  },
  decreasing(e: any) {
    let index = e.currentTarget.dataset.index
    this.data.shopList.forEach((item) => {
      this.data.totalMoney -= item.goodList[index].price
      item.goodList[index].count--
    });
    this.setData({
      totalMoney: this.data.totalMoney,
      shopList:this.data.shopList
    })
  },
  incremental(e: any) {
    let index = e.currentTarget.dataset.index
    this.data.shopList.forEach((item) => {
      this.data.totalMoney += item.goodList[index].price
      item.goodList[index].count++
    });
    this.setData({
      totalMoney: this.data.totalMoney,
      shopList:this.data.shopList
    })
  }
})
