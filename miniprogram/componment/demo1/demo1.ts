import { ComponentWithStore } from "mobx-miniprogram-bindings"
import { numStore } from "../../stores/numStroe"
ComponentWithStore({
  storeBindings: {
    store: numStore,
    fields: ["numA", "numB", "numComputed"],
    // actions: {
    //   update: "update",
    // },
    actions:['update']
  },
  /**
   * 组件的属性列表
   */
  properties: {
    demo: {
      type: String,
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    dd: 0
  },
  observers: {


  },
  /**
   * 组件的方法列表
   */
  methods: {
    updateHandle(this:any){
      console.log(999);
      
      this.update()
    }
  }
})
